# Copyright 2012 Arne Janbu
# Distributed under the terms of the GNU General Public License v2

require meson [ meson_minimum_version=0.52.1 ]

export_exlib_phases src_prepare src_test

SUMMARY="The Wayland display protocol library (client and server)"
HOMEPAGE="https://wayland.freedesktop.org/"

SLOT="0"
LICENCES="MIT"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        dev-libs/libxml2:2.0
        doc?
        (
            app-doc/doxygen[>=1.6.0]
            app-text/xmlto
            media-gfx/graphviz[>=2.26.0]
        )
        virtual/pkg-config
    build+run:
        dev-libs/expat
        dev-libs/libffi:=
        !x11-dri/mesa[<18.0.3] [[
            description = [ wayland imported libwayland-egl from mesa ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_TEST_PARAMS=( -j1 )

MESON_SRC_CONFIGURE_PARAMS=(
    -Dlibraries=true
    -Ddtd_validation=true
    -Dscanner=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc documentation'
)

wayland_src_prepare() {
    meson_src_prepare

    # Would be nice if meson had --docdir, upstream issue #825
    edo sed -e "s/meson.project_name()/& + '-${PVR}'/" -i doc/meson.build
}

wayland_src_test() {
    esandbox allow_net unix:/tmp/wayland-tests-*/wayland-*

    # Or else the path is too long for the test buffer
    XDG_RUNTIME_DIR=/tmp meson_src_test

    esandbox disallow_net unix:/tmp/wayland-tests-*/wayland-*
}

