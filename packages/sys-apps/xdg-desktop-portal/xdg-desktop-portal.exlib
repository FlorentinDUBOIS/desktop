# Copyright 2017-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='flatpak' release=${PV} suffix='tar.xz' ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require test-dbus-daemon

export_exlib_phases src_prepare

SUMMARY="A portal frontend service for Flatpak"
DESCRIPTION="
xdg-desktop-portal works by exposing a series of D-Bus interfaces known as
portals under a well-known name and object path. The portal interfaces include
APIs for file access, opening URIs, printing and others.
Flatpak grants sandboxed applications talk access to names in the
org.freedesktop.portal.* prefix. One possible way to use the portal APIs is
thus just to make D-Bus calls. For many of the portals, toolkits (e.g. GTK+)
are expected to support portals transparently if you use suitable high-level
APIs.
To actually use most portals, xdg-desktop-portal relies on a backend that
provides implementations of the org.freedesktop.impl.portal.* interfaces. One
such backend is provided by xdg-desktop-portal-gtk. Another one is in
development here: xdg-desktop-portal-kde."

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    geolocation [[ description = [ Support for location portal (needs geoclue) ] ]]
    screencast [[ description = [ Enable the screencast D-Bus interface ( portal ) ] ]]
    ( linguas: cs da de en_GB es fr gl hr hu id it lt pl pt_BR ru sk sr sv tr uk zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.3
        app-text/xmlto
        sys-apps/flatpak[>=1.5.0]
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config[>=0.24]
    build+run:
        core/json-glib
        dev-libs/glib:2
        !sys-apps/flatpak[<0.11] [[
            description = [ file collisions, document portal was moved into xdg-desktop-portal ]
            resolution = upgrade-blocked-before
        ]]
        sys-fs/fuse:0
        geolocation? ( gps/geoclue:2.0[>=2.5.2] )
        screencast? ( media/pipewire[>=0.2.90] )
    run:
        sys-apps/bubblewrap
    suggestion:
        kde/xdg-desktop-portal-kde  [[
            description = [ Backend providing integration with the Plasma desktop ]
        ]]
        sys-apps/xdg-desktop-portal-gtk [[
            description = [ Backend providing integration with GTK desktops such as GNOME ]
        ]]
        sys-apps/xdg-desktop-portal-wlr [[
            description = [ Backend providing integration with wlroots-based WMs such as sway ]
        ]]
"

xdg-desktop-portal_src_prepare() {
    # Fails due to missing permissions to mount something via fuse
    edo sed -e "/test_programs += test-portals/d" -i tests/Makefile.am.inc

    autotools_src_prepare
}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-coverage
    # Unwritten, needed for tests: https://github.com/matthiasclasen/libportal
    --disable-libportal
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'geolocation geoclue'
    'screencast pipewire'
)

