# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require meson \
    bash-completion \
    option-renames [ renames=[ 'gtk3 gtk' 'systemd providers:systemd' ] ] \
    systemd-service \
    udev-rules \
    zsh-completion \
    gsettings

export_exlib_phases src_install

SUMMARY="A sound server for POSIX and Win32 systems"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/PulseAudio"
DOWNLOADS="https://freedesktop.org/software/${PN}/releases/${PNV}.tar.xz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/Notes/${PV} [[ lang = en ]]"

LICENCES="
    AGPL-3 [[ note = [ src/utils/qpaeq ] ]]
    LGPL-2.1
    MIT [[ note = [ src/modules/reserve{-monitor,}.{c,h} src/modules/rtkit.{c,h} ] ]]
    g711 [[ note = [ src/pulsecore/g711.c ] ]]
    adrian-license [[ note = [ src/modules/echo-cancel/* ] ]]
    as-is [[ note = [ src/pulsecore/g711.h ] ]]

    bluetooth? (
        GPL-2 [[ note = [ src/modules/bluetooth/{a2dp-codecs.h,proximity-helper.c}, everything using
                          the bluetooth module (libpulsecore and everything linking against it), see LICENSE  ] ]]
        LGPL-2.1 [[ note = [ the pulseaudio client lib (libpulse), see LICENSE ] ]]
    )
"
SLOT="0"
MYOPTIONS="
    X [[ description = [ Enable X session integration ] ]]
    async-dns [[ description = [ Support for asynchronous name service queries ] ]]
    avahi
    bluetooth
    equalizer [[ description = [ Modules which need equalizer functions ] ]]
    gstreamer [[ description = [ Enable optional GStreamer-based RTP support ] ]]
    gtk
    jack
    oss
    tcpd
    webrtc-aec [[ description = [ Alternative to speex for echo cancellation, has higher quality ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: consolekit elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.20]
    build+run:
        group/audio
        group/pulse
        group/pulse-access
        group/pulse-rt
        user/pulse
        dev-libs/glib:2[>=2.28.0]
        dev-libs/orc:0.4[>=0.4.16] [[
            note = [ dev-libs/orc-0.4.15 is known to cause freezes, see https://bugs.freedesktop.org/show_bug.cgi?id=41589 ]
        ]]
        media-libs/libsndfile[>=1.0.20]
        media-libs/soxr[>=0.1.1]
        media-libs/speexdsp
        sys-apps/dbus[>=1.4.12][providers:consolekit?][providers:elogind?][providers:systemd?]
        sys-devel/libtool[>=2.4]
        sys-libs/gdbm
        sys-libs/libcap
        sys-sound/alsa-lib[>=1.0.19]
        X? (
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libX11[xcb(+)]
            x11-libs/libxcb[>=1.6]
            x11-libs/libXtst
            x11-utils/xcb-util
        )
        async-dns? ( net-libs/libasyncns[>=0.1] )
        avahi? ( net-dns/avahi[>=0.6.0][dbus] )
        bluetooth? (
            media-libs/sbc[>=1.0]
            net-wireless/bluez[>=5.0]
        )
        equalizer? ( sci-libs/fftw )
        gstreamer? ( 
            media-libs/gstreamer:1.0[>=1.14][gobject-introspection]
            media-plugins/gst-plugins-base:1.0
        )
        gtk? ( x11-libs/gtk+:3[>=3.0][X] )
        jack? ( media-sound/jack-audio-connection-kit[>=0.117] )
        providers:elogind? ( sys-auth/elogind[>=165] )
        providers:eudev? ( sys-apps/eudev )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        providers:systemd? ( sys-apps/systemd[>=165] )
        tcpd? ( sys-apps/tcp-wrappers )
        webrtc-aec? ( media-libs/webrtc-audio-processing[>=0.2] )
    run:
        providers:consolekit? ( sys-auth/ConsoleKit2 )
    test:
        dev-libs/check[>=0.9.10]
    recommendation:
        sys-sound/alsa-plugins[pulseaudio]
    suggestion:
        media-sound/pavucontrol [[ description = [ Provides a simple GTK based volume mixer tool ] ]]
        media-sound/pulseaudio-dlna [[ description = [ Stream to DLNA/UPNP and Chromecast devices via PulseAudio ] ]]
        sys-sound/oss [[ description = [ Provides an alternative sound architecture instead of ALSA ] ]]
        sound-themes/sound-theme-freedesktop [[ description = [ Sample PulseAudio event sounds mentioned in default.pa ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Daccess_group=pulse-access
    -Ddaemon=true
    -Ddoxygen=false
    -Dlocalstatedir=/var
    -Dadrian-aec=false # alternative to speex
    -Dalsa=enabled # udev needs alsa or oss support
    -Dbluez5-gstreamer=disabled
    -Dbluez5-ofono-headset=false
    -Ddatabase=gdbm # alternative tdb or simple
    -Ddbus=enabled # recommended by upstream
    -Dgcov=false
    -Dglib=enabled
    -Dgsettings=enabled # We already depend on glib anyway and this just builds a module
    -Dhal-compat=true
    -Dipv6=true
    -Dlegacy-database-entry-format=true
    -Dlirc=disabled
    -Dman=true
    -Dopenssl=enabled
    -Dorc=enabled
    -Drunning-from-build-tree=true
    -Dsamplerate=disabled
    -Dsoxr=enabled # will eventually replace speex as the default resampler
    -Dspeex=enabled # recommended and default resampler
    -Dstream-restore-clear-old-devices=true
    -Dsystem_group=pulse
    -Dsystem_user=pulse
    -Dsystemduserunitdir=${SYSTEMDUSERUNITDIR}
    -Dudev=enabled
    -Dudevrulesdir=${UDEVRULESDIR}
    -Dvalgrind=disabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'X x11'
    'async-dns asyncns'
    avahi
    'bluetooth bluez5'
    'equalizer fftw'
    gstreamer
    'gtk gtk3'
    jack
    'oss oss-output'
    'providers:elogind elogind'
    'providers:systemd systemd'
    'tcpd tcpwrap'
    webrtc-aec
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bash-completion bashcompletiondir /usr/share/bash-completion/completions no'
    'bluetooth bluez5-native-headset'
    'zsh-completion zshcompletiondir /usr/share/zsh/site-functions no'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

pulseaudio_src_install() {
    meson_src_install

    # keep the administrator's override directives directories
    keepdir /etc/pulse/{client,daemon}.conf.d

    insinto /etc/security/limits.d/
    doins "${FILES}"/pulse-rt.conf

    keepdir /usr/$(exhost --target)/libexec/pulse

    option providers:systemd || option providers:elogind && edo sed \
        -e '/module-console-kit/,/.endif/{ /^#/!{ s/^/#/ } }' \
        -i "${IMAGE}"/etc/pulse/default.pa
}

