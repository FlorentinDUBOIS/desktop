# Copyright 2015 Niels Ole Salscheider <niels_ole@salscheider-online.de>
# Distributed under the terms of the GNU General Public License v2

SIGNON_DBUS_SPECIFICATION_REV="67487954653006ebd0743188342df65342dc8f9b"

require gitlab [ user=accounts-sso tag=VERSION_${PV} suffix=tar.bz2 ]
require vala [ vala_dep=true with_opt=true option_name=gobject-introspection ]
require meson test-dbus-daemon

export_exlib_phases src_prepare src_install

SUMMARY="Single signon authentication library for GLib applications"
DOWNLOADS+="
    https://gitlab.com/accounts-sso/signon-dbus-specification/repository/archive.tar.bz2?ref=${SIGNON_DBUS_SPECIFICATION_REV} -> signon-dbus-specification-${SIGNON_DBUS_SPECIFICATION_REV}.tar.bz2
"

LICENCES="LGPL-2.1"
SLOT="0"
#    doc - Isn't optional anymore with meson
MYOPTIONS="
    doc
    gobject-introspection
    python
"

DEPENDENCIES="
    build:
        dev-doc/gtk-doc
        dev-lang/python:*
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/glib:2[>=2.36]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
        python? ( gnome-bindings/pygobject:3[>=2.90] )
    run:
        net-libs/signon[>=8.40]
    test:
        dev-libs/check[>=0.9.4]
"

MESON_SOURCE=${WORKBASE}/${PN}-VERSION_${PV}-7f9f10a2e7533b74eaf027b26de48818326a1dcf

#    'doc documentation'
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    python
)
MESON_SRC_CONFIGURE_TESTS=( '-Dtests=true -Dtests=false' )

libsignon-glib_src_prepare() {
    meson_src_prepare

    # signon-dbus-specification submodule
    edo rmdir libsignon-glib/interfaces
    edo ln -s \
        "${WORKBASE}"/signon-dbus-specification-${SIGNON_DBUS_SPECIFICATION_REV}-${SIGNON_DBUS_SPECIFICATION_REV} \
        libsignon-glib/interfaces
}

libsignon-glib_src_install() {
    meson_src_install

    ! option doc && edo rm -r "${IMAGE}"/usr/share/gtk-doc
}

