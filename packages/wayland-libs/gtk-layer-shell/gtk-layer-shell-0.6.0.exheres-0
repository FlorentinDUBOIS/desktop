# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=wmww tag=v${PV} ] meson

SUMMARY="A library to create panels and other desktop components for Wayland"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="gobject-introspection"

DEPENDENCIES="
    build:
        virtual/pkg-config
        sys-libs/wayland-protocols[>=1.16.0]
    build+run:
        sys-libs/wayland[>=1.10.0]
        x11-libs/gtk+:3[gobject-introspection?][wayland]
"

# Lots of tests fail because starting the wayland server times out
# With `-Dtests=false` a single test is added to the testsuite which will always fail and show that
# tests weren't enabled
RESTRICT=test

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-build-Make-generating-introspection-data-optional.patch
)
MESON_SRC_CONFIGURE_PARAMS=(
    -Ddocs=false
    -Dexamples=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
)
MESON_SRC_CONFIGURE_TESTS=( '-Dtests=true -Dtests=false' )

